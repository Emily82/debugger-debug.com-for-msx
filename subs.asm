; **** Subroutines for DEBUG.COM (ver.0.52) ****
;
;	Author: Copyright (C) Ariakirasoft Software, assembler
;		logic and bugfixes written by Emily82 (Dec. 2019)
;		Many thanks to GuyveR800 for his patience and big help.
;
; reg_view	( 15)
; reg_save	( 67)
; reg_load	( 91)
; put_filename	(116)
; load_file	(158)
; write_file	(217)
; get_sector	(282)
; unasm_main	(328)
; get_codelen	(359)
; unasm_nym	(488)
; asm_main	(1166)


reg_view:
	ld	hl,reg_area
	ld	de,reg_mes
	ld	b,10
reg_v2:	call	putlin
	ld	c,(hl)
	inc	hl
	ld	a,(hl)
	inc	hl
	call	puthex
	ld	a,c
	call	puthex
	call	putspc
	call	putspc
	djnz	reg_v2
	call	putlin
	ld	hl,0
	add	hl,sp
	ld	a,h
	call	puthex
	ld	a,l
	call	puthex
	call	putlin
	ld	hl,reg_area	; F reg
	ld	bc,2000h+'N'	; B:spc,C:'N'
	bit	6,(hl)	; Z
	jr	nz,reg_vz
	print	c
reg_vz:	print	'Z'
	print	b
	bit	0,(hl)	; Cy
	jr	nz,reg_vc
	print	c
reg_vc:	print	'C'
	print	b
	print	'P'
	ld	a,'E'
	bit	2,(hl)
	jr	nz,reg_vp
	add	a,'O'-'E'
reg_vp:	call	putch
	print	b
	bit	7,(hl)
	ld	a,'M'
	jr	nz,reg_vs
	add	a,'P'-'M'
reg_vs:	call	putch
	print	')'
	call	putret
	jp	comlin


reg_save:
	exx
	ex	af,af'
	push	iy
	push	hl
	push	de
	push	bc
	push	af
	exx
	ex	af,af'
	push	ix
	push	hl
	push	de
	push	bc
	push	af
	ld	hl,0
	add	hl,sp
	ld	de,reg_area
	ld	bc,20
	ldir
	ld	sp,hl
	ret


reg_load:
	ld	hl,-20
	add	hl,sp
	ld	sp,hl
	ex	de,hl
	ld	hl,reg_area
	ld	bc,20
	ldir
	pop	af
	pop	bc
	pop	de
	pop	hl
	pop	ix
	exx
	ex	af,af'
	pop	af
	pop	bc
	pop	de
	pop	hl
	pop	iy
	exx
	ex	af,af'
	ret


put_filename:
	ld	a,'"'
	call	putch
	ld	hl,fcb
	ld	a,(hl)
	inc	hl
	or	a
	jr	z,name_put2
	add	a,'@'
	call	putch
	ld	a,':'
	call	putch
name_put2:
	ld	b,8
name_put3:
	ld	a,(hl)
	inc	hl
	cp	32
	jr	z,name_put4
	call	putch
	djnz	name_put3
name_put4:
	ld	hl,fcb+9
	ld	a,(hl)
	cp	32
	jr	z,name_put6
	ld	a,'.'
	call	putch
	ld	b,3
name_put5:
	ld	a,(hl)
	inc	hl
	cp	32
	jr	z,name_put6
	call	putch
	djnz	name_put5
name_put6:
	ld	a,'"'
	call	putch
	ret


load_file:
	ex	de,hl
file_load:			; DE:DMA  005c:FCB
	push	de
	ld	c,1ah
	call	5
	call	put_filename
	ld	de,fcb
	ld	c,0fh
	call	5
	pop	de
	inc	a
	jr	z,load_err1	; not found
	ld	hl,1
	ld	(fcb+14),hl
	dec	hl
	ld	(fcb+33),hl
	ld	(fcb+35),hl
	ld	hl,(fcb+18)
	ld	a,h
	or	l
	jr	nz,load_err2	; too large
	ld	hl,(fcb+16)
	dec	hl
	add	hl,de
	jr	c,load_err2
	call	putspc
	call	putspc
	ld	a,d
	call	puthex
	ld	a,e
	call	puthex
	ld	a,'-'
	call	putch
	ld	a,h
	call	puthex
	ld	a,l
	call	puthex
	call	putspc
	ld	hl,(fcb+16)
	call	put10
	ld	de,fcb
	ld	c,27h
	call	5
	ld	(reg_area+2), hl	; save to reg_area BC
	ld	de,load_mes
	call	putlin
	ld	de,mes_ed
load_3:	call	putlin
	println
	jp	comlin
load_err1:
	ld	de,load_errmes1
	jr	load_3
load_err2:
	ld	de,load_errmes2
	jr	load_3


write_file:		; BC:start HL:end
	push	hl
	or	a
	sbc	hl,bc
	pop	de
	jp	c,error	; start>end
	inc	hl	; HL:length
	push	hl
	push	de
	push	bc
	call	put_filename
	pop	bc
	pop	de
	pop	hl
	call	putspc
	call	putspc
	ld	a,b
	call	puthex
	ld	a,c
	call	puthex
	ld	a,'-'
	call	putch
	ld	a,d
	call	puthex
	ld	a,e
	call	puthex
	call	putspc
	call	put10
	push	hl
	ld	de,fcb
	ld	c,13h
	call	5	; delete
	ld	de,fcb
	ld	c,16h
	call	5	; make
	or	a
	jr	nz,write_error
	ld	hl,0
	ld	(fcb+35),hl
	ld	(fcb+33),hl
	inc	hl
	ld	(fcb+14),hl
	pop	hl
	ld	de,fcb
	ld	c,026h
	call	5	; write
	or	a
	jr	nz,write_error
	ld	de,fcb
	ld	c,10h	; close
	call	5
	ld	de,write_mes
	call	putlin
	ld	de,mes_en+1
	call	putlin
	println
	jp	comlin
write_error:
	pop	hl
	ld	de,write_errmes
	call	putlin
	jp	comlin


get_sector:			; CY on:OK off:error
	call	getwrd
	or	a
	ret	z	; no more
	cp	255
	ret	z	; no drive
	ld	a,h
	or	a
	ret	nz	; over 255
	ld	a,l
	cp	8
	ret	nc	; over 7
	push	de
	 push	hl
	  ld	c,18h
	  call	5
	 pop	bc
	 ld	a,c
	 inc	a
get_sec_loop:
	 rr	l
	 dec	a
	 jr	nz,get_sec_loop
	pop	de
	ret	nc	; not online
	push	bc
	 call	getwrd	; HL:sector
	pop	bc	; C:drive
	or	a
	ret	z	; no more
	cp	255
	ret	z	; no sector
	push	hl
	 push	bc
	  call	getwrd	; get length
	 pop	bc
	 ld	h,l	; H:length
	 ld	l,c	; L:drive
	pop	bc
	cp	255
	ret	z	; no length
	ld	e,c
	ld	d,b	; DE:sector
	scf
	ret		; L:drive DE:sector H:length CY=1

; ** UNASM LOGIC **

unasm_main:
	ld	a,h
	call	puthex
	ld	a,l
	call	puthex
	call	putspc
	call	putspc
	push	hl
	 call   get_codelen	; > B:len (1-4,b7=1>error)
	pop	hl
	ld	c,b
	res	7,b
	push	hl
unasm_putcode:
	ld	a,(hl)
	inc	hl
	call	puthex
	djnz	unasm_putcode
	pop	hl
	call	puttab
	bit	7,c
	jr	nz,unasm_error
	jp	unasm_nym
unasm_error:
	res	7,c
	ld	b,0
	add	hl,bc
	ld	de, err
	jp	putlin


get_codelen:
	ld	a,(hl)
	samejr	0ddh,get_code_xy	; DD
	samejr	0fdh,get_code_xy	; FD
	samejp	0edh,code_ed		; ED
get_co:	cp	040h
	jp	c,code_00		; 00-3F
	cp	0c0h
	ld	b,1
	ret	c			; 40-BF
	jr	code_c0			; C0-FF
get_code_xy:
	inc	hl
	ld	a,(hl)
	ld	b,2+128
	sameret	0ddh
	sameret	0edh
	sameret	0fdh
	push	af
	call	get_co
	pop	af
	inc	b
	sameret	39h
	push	af			; CHANGE: added support for ixh,ixl,iyh,iyl
	and	0fh
	samejr	4h, colx0
	samejr	5h, colx0
	samejr	0Ch, colx0
	samejr	0Dh, colx0
	pop	af
	jr	get_code_xy2
colx0:
	pop	af
	and	0f0h
	sameret	40h
	sameret	50h
	sameret	80h
	sameret	90h
	sameret	0A0h
	sameret	0B0h
get_code_xy2:
	cp	60h
	jr	nc, row60
	jr	get_code_xy3
row60:
	cp	66h
	ret	c
get_code_xy3:
	cp	67h
	jr	nc, row67
	jr	get_code_xy4
row67:
	cp	6Eh
	ret	c
get_code_xy4:
	sameret	06Fh
	sameret	07Ch
	sameret	07Dh
	cp	30h
	ret	c
	cp	0e0h
	ret	nc
	inc	b
	ret
code_c0:
	ld	b,3
	sameret	0c3h	; C3
	sameret	0cdh	; CD
	and	7
	sameret	2	; x2,xA
	ld	b,1
	ret	c	; x0,x1,x8,x9
	ld	b,3
	sameret	4	; x4,xC
	dec	b
	sameret	6	; x6,xE
	dec	b
	ret	nc	; x7,xF
	sameret	5	; x5
	bit	5,(hl)
	ret	nz	; E3,EB,F3,FB
	inc	b
	ret		; D3,DB
code_00:
	and	7
	ld	b,2
	cp	6
	ret	z	; x6,xE
	dec	b
	cp	3
	ret	nc	; x3,x4,x5,x7,xB,xC,xD,xF
	cp	1
	jr	z,code01
	jr	c,code00
	bit	5,(hl)
	ret	z	; 02,0A,12,1A
	ld	b,3
	ret		; 22,2A,32,3A
code01:	bit	3,(hl)
	ret	nz	; 09,19,29,39
	ld	b,3
	ret		; 01,11,21,31
code00:	ld	a,(hl)
	cp	10h
	ret	c	; 00,08
	inc	b
	ret		; 10,18,20,28,30,38
code_ed:
	inc	hl
	ld	a,(hl)		; 2nd byte
	ld	c,a
	ld	b,2+128
	cp	40h
	ret	c		; ED 0x,1x,2x,3x
	cp	0c0h
	jr	nc,code_ed_c0	; ED Cx,Dx,Ex,Fx
	cp	0a0h
	jr	nc,code_ed_a0	; ED Ax,Bx
	cp	080h
	ret	nc		; ED 8x,9x
	;sameret	71h	; ED 71		(CHANGED: OUT (C),0 is supported)
	and	7
	ld	b,2
	cp	3
	ret	c	; ED 40,41,42,48,,,,,,72,78,79,7A
	ld	b,4
	ret	z	; ED 43,4B,53,5B,63,6B,73,7B
	ld	a,c
	ld	b,2
	cp	48h
	ret	c	; ED 44,45,46,47
	ld	b,2+128
	cp	70h
	ret	nc	; ED 74,75,76,77,7C,7D,7E,7F
	and	7
	sameret	4	; ED 4C,54,5C,64,6C
	ld	b,2
	sameret	7	; ED 4F,57,5F,67,6F
	ld	a,c
	sameret	4dh	; ED 4D
	sameret	56h	; ED 56
	sameret	5eh	; ED 5E
	ld	b,2+128
	ret		; ED 4E,55,5D,65,66,6D,6E
code_ed_c0:
	and	15
	ld	b,2
	cp	1
	ret	z	; ED C1
	cp	3
	ret	z	; ED C3
	cp	9
	ret	z	; ED C9
	ld	b,2+128
	ret
code_ed_a0:
	and	7
	ld	b,2
	cp	4
	ret	c
	ld	b,2+128
	ret


unasm_nym:
	ld	a,(hl)
	ld	(@ixiy),a
unasm3:	ld	a,(hl)
	inc	hl
	ld	c,a
	cp	040h
	jr	c,unasm4
	cp	080h
	jp	c,un@40
	sub	0c0h
	jp	c,un@80
	and	0fh
	add	a,10h
	jr	unasm5
unasm4:	and	0fh
unasm5:	push	hl
	ld	hl,unadrs
	add	a,a
	add	a,l
	ld	l,a
	ld	a,h
	adc	a,0
	ld	h,a
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	pop	hl
	ld	a,c
	push	de
	ret		; jp (de)
un1end:	call	putlin
unend:	call	putret
	ret

un@40:				;40-7F:	LD HALT
	ld	de,_halt
	cp	76h
	jp	z,un1end
	call	ldnym
	call	putr
	call	putcon
	ld	a,c
	call	putreg
	jp	unend

un@80:	and	38h		;80-BF:	ADD a,r ... CP r
	rrca
	ld	de,_math
	add	a,e
	ld	e,a
	ld	a,d
	adc	a,0
	ld	d,a
	call	lintab
	ld	a,c
	cp	0a0h
	jr	nc,un@80c
	sub	090h
	cp	008h
	jr	c,un@80c
un@80b:	push	af
	ld	de,_acon
	call	putlin
	pop	af
un@80c:	call	putreg
	jp	unend

unnop:	rrca				;00:08:	NOP EX DJNZ JR
	rrca
	rrca
	ld	de,_nop
	and	7
	jp	z,un1end		; nop
	ld	de,_exaf
	dec	a
	jp	z,un1end		; ex af,af'
	ld	de,_djnz
	dec	a
	jr	z,unnop2
	ld	de,_jr
unnop2:	call	lintab
	bit	5,c
	jr	z,unnop4
	res	5,c
	call	putcc			; jr cc,e
	call	putcon
unnop4:	ld	a,(hl)			; jr e ; djnz e
	inc	hl
	ld	d,a
unlop5:	add	a,l
	ld	e,a
	ld	a,h
	adc	a,0
	bit	7,d
	jr	z,unlop6
	dec	a
unlop6:	call	puthex
	ld	a,e
	call	puthex
	;ld	a,'h'
	;call	putch
	jp	unend

unld2:	call	ldnym			;01:	LD rr,nn
	call	putrr
	call	putcon
	call	putad
	jp	unend

unld3:	call	ldnym			;02:	LD(rr),a LD(nn),hl LD(nn),a
	ld	a,'('
	call	putch
	ld	a,c
	cp	20h
	push	af
	call	c,putrr		; LD (BC),A	LD (DE),A
	pop	af
	call	nc,putad	; LD (nn),HL	LD (nn),A
	ld	a,')'
	call	putch
	call	putcon
	ld	a,c
	cp	22h
	jr	z,unld3g
	ld	a,'A'
	call	putch
	jr	unld3h
unld3g:
	call	putrr
unld3h:
	jp	unend

uninc2:	ld	de,_inc			;03:	INC rr
	jr	unded2
undec2:	ld	de,_dec			;0B:	DEC rr
unded2:	call	lintab
	call	putrr
	jp	unend

uninc1:	ld	de,_inc			;04:	INC r
	jr	unded1
undec1:	ld	de,_dec			;05:	DEC r
unded1:	call	lintab
	call	putr
	jp	unend

unld4:	call	ldnym			;05:	LD r,n
	call	putr
	call	putcon
	ld	a,(hl)
	inc	hl
	call	puthex
	;ld	a,'h'
	;call	putch
	jp	unend

unrlca:	rrca				;07:0F	RLCA ... RRA DAA CPL SCF CCF
	rrca
	rrca
	and	7
	ld	d,a
	add	a,a
	add	a,a
	add	a,d
	ld	de,_rlca
	add	a,e
	ld	e,a
	ld	a,d
	adc	a,0
	ld	d,a
	jp	un1end

unadd2:	ld	de,_math		;09:	ADD hl,rr
	call	lintab
	ld	a,29h
	call	putrr@
	call	putcon
	call	putrr
	jp	unend

unld5:	call	ldnym			;0A:	LD a,(bc) a,(de) hl,(nn) a,(nn)
	ld	a,c
	cp	2ah
	jr	z,unld5c
	ld	a,'A'
	call	putch
	jr	unld5e
unld5c:					; BUGFIX: LD ix,(nn) and iy,(nn) didnt work
	call	putrr
unld5e:
	call	putcon
	ld	a,'('
	call	putch
	ld	a,c
	cp	20h
	push	af
	call	c,putrr
	pop	af
	call	nc,putad
	ld	a,')'
	call	putch
	jp	unend

unretc:	ld	de,_ret		;C0:C8	RET cc
	call	lintab
	call	putcc
	jp	unend

unpop:	ld	de,_pop		;C1	POP
	call	lintab
	call	putrr
	jp	unend

unjpcc:	ld	de,_jp		;C2:CA	JP cc,nn
	jr	unjccc
uncalc:	ld	de,_call	;C4:CC	CALL cc,nn
unjccc:	call	lintab
	call	putcc
	call	putcon
	call	putad
	jp	unend

unjp:	and	38h			;C3:CB:	JP CB; OUT IN EX DI EI
	rrca
	rrca
	rrca
	dec	a
	jr	z,un@cb
	dec	a
	jr	z,un@d3
	dec	a
	jr	z,un@db
	dec	a
	jr	z,un@e3
	ld	de,_exdh
	dec	a
	jp	z,un1end
	ld	de,_di
	dec	a
	jp	z,un1end
	ld	de,_ei
	dec	a
	jp	z,un1end
un@c3:	ld	de,_jp	; JP
	call	lintab
	call	putad
	jp	unend
un@d3:	ld	de,_out	; OUT (n),a
	call	lintab
	ld	a,'('
	call	putch
	ld	a,(hl)
	inc	hl
	call	puthex
	ld	de,_out2
	jp	un1end
un@db:	ld	de,_in	; IN a,(n)
	call	lintab
	ld	de,_acon
	call	putlin
	ld	a,'('
	call	putch
	ld	a,(hl)
	inc	hl
	call	puthex
	;ld	a,'h'
	;call	putch
	ld	a,')'
	call	putch
	jp	unend
un@e3:	ld	de,_exsh	;EX (sp),hl
	call	putlin
	call	putrr
	jp	unend
un@cb:	ld	a,(@ixiy)	;RLC ... BIT SET RES
	cp	0ddh
	jr	z,un@cb1
	cp	0fdh
	jr	z,un@cb1
	ld	a,(hl)
	inc	hl
	jr	un@cb2
un@cb1:	inc	hl
	ld	a,(hl)
	dec	hl
un@cb2:	ld	c,a
	cp	40h
	jr	nc,unbit
	and	38h
	rrca
	ld	de,_rlc
	add	a,e
	ld	e,a
	ld	a,d
	adc	a,0
	ld	d,a
	call	lintab
	ld	a,c
	call	putreg
	jr	un@cb8
unbit:	and	0c0h
	rrca
	rrca
	rrca
	rrca
	ld	de,_bit-4
	add	a,e
	ld	e,a
	ld	a,d
	adc	a,0
	ld	d,a
	call	lintab
	ld	a,c
	and	38h
	rrca
	rrca
	rrca
	add	a,'0'
	call	putch
	call	putcon
	ld	a,c
	call	putreg
un@cb8:	ld	a,(@ixiy)
	cp	0ddh
	jr	z,un@cb9
	cp	0fdh
	jr	z,un@cb9
	jp	unend
un@cb9:	inc	hl
	jp	unend

unpush:	ld	de,_push		;C5:	PUSH
	call	lintab
	call	putrr
	jp	unend

unadd3:	ld	a,c			;C6:CE:	ADD a,n ... CP n
	and	38h
	rrca
	ld	de,_math
	add	a,e
	ld	e,a
	ld	a,d
	adc	a,0
	ld	d,a
	call	lintab
	ld	a,c
	cp	0d6h
	jr	z,unsub3
	cp	0e6h
	jr	nc,unsub3
	ld	a,'A'
	call	putch
	ld	a,','
	call	putch
unsub3:	ld	a,(hl)
	inc	hl
	call	puthex
	;ld	a,'h'
	;call	putch
	jp	unend

unrst:	ld	de,_rst			;C7:CF:	RST
	call	lintab
	ld	a,c
	and	38h
	call	puthex
	;ld	a,'h'
	;call	putch
	jp	unend

unret:	ld	de,_ret			;C9:	RET EXX JP(hl) LDsp,hl
	cp	0c9h
	jp	z,un1end; ret
	ld	de,_exx
	cp	0d9h
	jp	z,un1end; exx
	cp	0e9h
	jr	nz,un@f9
	ld	de,_jp	; jp (hl)
	call	lintab
	ld	a,'('
	call	putch
	call	putrr
	ld	a,')'
	call	putch
	jp	unend
un@f9:	ld	de,_ldsh; ld sp,hl
	call	putlin
	ld	a,0e9h
	call	putrr@
	jp	unend

uncall:	cp	0edh			;CD:	CALL IX; IY; { ED; }
	jr	z,un@ed
	cp	0cdh
	jp	nz,unasm3
	ld	de,_call; call
	call	lintab
	call	putad
	jp	unend

un@ed:	ld	a,(hl)			;ED:	...
	inc	hl
	ld	c,a
	cp	0c0h
	jp	nc,unmul	; MULUB A,r / MULUW HL,rr
	cp	0a0h
	jr	nc,unldi	; LDI / CPI / -- / INDR / OTDR
	and	7
	jp	z,unin		; IN r,(C)
	dec	a
	jp	z,unout		; OUT (C),r
	dec	a
	jp	z,unsbc		; SBC HL,rr / ADC HL,rr
	dec	a
	jp	z,unld6		; LD (nn),rr / LD rr,(nn)
	dec	a
	jp	z,unneg		; NEG
	dec	a
	jp	z,unretn	; RETN / RETI
	dec	a
	jp	z,unim		; IM 0 / IM 1 / IM 2
unld8:	bit	5,c
	jr	nz,unrrd	; RRD / RLD
	call	ldnym
	ld	a,'A'		; LD I,A / LD R,A / LD A,I / LD A,R
	bit	4,c
	jr	nz,unld8b
	ld	a,'I'
	bit	3,c
	jr	z,unld8b
	ld	a,'R'
unld8b:	call	putch
	ld	a,','
	call	putch
	ld	a,'A'
	bit	4,c
	jr	z,unld8c
	ld	a,'I'
	bit	3,c
	jr	z,unld8c
	ld	a,'R'
unld8c:	call	putch
	jp	unend
unrrd:	ld	a,'R'
	call	putch
	ld	a,'R'
	bit	3,c
	jr	z,unrrd@
	ld	a,'L'
unrrd@:	call	putch
	ld	a,'D'
	call	putch
	jp	unend
unldi:	cp	0a3h	; ldi - otdr
	jr	z,unldi1
	cp	0abh
	jr	nz,unldi2
unldi1:	inc	a
unldi2:	and	7
	ld	e,a
	add	a,a
	add	a,e
	ld	de,_ld
	add	a,e
	ld	e,a
	ld	a,d
	adc	a,0
	ld	d,a
	call	putlin
	ld	a,'I'
	bit	3,c
	jr	z,unldi3
	ld	a,'D'
unldi3:	call	putch
	bit	4,c
	jp	z,unend
	ld	a,'R'
	call	putch
	jp	unend
unin:	ld	de,_in
	call	lintab
	ld	de,_reg
	ld	a,c
	rrca
	rrca
	rrca
	and	7
	ld	b,a
	samejr	0, unin2	; BUGFIX: if 0 no need to inc de
unin1:	inc	de
	djnz	unin1
unin2:
	ld	a,(de)
	call	putch
	ld	de,_in@
	jp	un1end
unout:	ld	de,_out@
	call	putlin
	ld	a, c		; CHANGE: implemented OUT (C),0
	rrca
	rrca
	rrca
	and	7
	cp	6
	jr	nz, unout2
	ld	a, '0'
	call	putch
	jp	unend
unout2:
	call	putr
	jp	unend
unsbc:	ld	de,_sbc@
	bit	3,c
	jr	z,unsbc2
	ld	de,_adc@
unsbc2:	call	putlin
	call	putrr
	jp	unend
unld6:	call	ldnym
	bit	3,c
	jr	nz,unld7
	ld	a,'('
	call	putch
	call	putad
	ld	a,')'
	call	putch
	ld	a,','
	call	putch
	call	putrr
	jp	unend
unld7:	call	putrr
	call	putcon
	ld	a,'('
	call	putch
	call	putad
	ld	a,')'
	call	putch
	jp	unend
unneg:	ld	de,_neg
	jp	un1end
unretn:	ld	de,_ret
	call	putlin
	ld	a,'N'
	bit	3,c
	jr	z,unren2
	ld	a,'I'
unren2:	call	putch
	jp	unend
unim:	ld	de,_im
	call	lintab
	ld	a,'0'
	bit	4,c
	jr	z,unim@
	inc	a
	bit	3,c
	jr	z,unim@
	inc	a
unim@:	call	putch
	jp	unend
unmul:	bit	1,c
	jr	nz,unmulw
	ld	de,_mulub
	call	putlin
	call	putr
	jp	unend
unmulw:	ld	de,_muluw
	call	putlin
	res	7,c
	call	putrr
	jp	unend


; ** UNASM SUB **

putr:	ld	a,c		; C [5-3]
putr@:	rrca			; A [5-3]
	rrca
	rrca
putreg:	and	7		; A [2-0]
	ex	af, af'
	ld	a,(@ixiy)
	samejr	0DDh, putregx
	samejr	0FDh, putregy
	ex	af, af'
putreg2:
	cp	6
	jr	z,puthl
	ld	de,_reg
	add	a,e
	ld	e,a
	ld	a,d
	adc	a,0
	ld	d,a
	ld	a,(de)
	call	putch
	ret
putregx:
	ex	af, af'
	samejr	4, putixh
	samejr	5, putixl
	jr	putreg2
putregy:
	ex	af, af'
	samejr	4, putiyh
	samejr	5, putiyl
	jr	putreg2
putixh:
	ld	de, _ixh
	call	putlin
	ret
putixl:
	ld	de, _ixl
	call	putlin
	ret
putiyh:
	ld	de, _iyh
	call	putlin
	ret
putiyl:
	ld	de, _iyl
	call	putlin
	ret

puthl:	ld	a,'('
	call	putch
	ld	a,20h
	call	putrr@
	ld	a,(@ixiy)
	cp	0ddh
	jr	z,putxy
	cp	0fdh
	jr	nz,puthl@
putxy:	ld	e,'+'
	ld	a,(hl)
	inc	hl
	bit	7,a
	jr	z,putxy@
	ld	e,'-'
	neg
putxy@:	push	af
	ld	a,e
	call	putch
	pop	af
	call	puthex
	;ld	a,'h'
	;call	putch
puthl@:	ld	a,')'
	jp	putch

putrr:	ld	a,c		; C [5-4]
putrr@:	bit	5,a		; A [5-4]
	jr	z,putrr0
	bit	4,a
	jr	z,putrr2
putrr5:	ld	e,5	  ; SP AF
	cp	0f0h
	jr	c,putrr6
	inc	e
putrr6:	ld	a,e
	jr	putrr9
putrr2:	ld	a,(@ixiy) ; HL IX IY
	ld	e,4
	cp	0fdh
	jr	z,putrr6
	dec	e
	cp	0ddh
	jr	z,putrr6
	dec	e
	jr	putrr6
putrr0:	rrca		 ; BC DE
	rrca
	rrca
	rrca
	and	1
putrr9:	ld	de,_rr
	jr	putcc_

putcc:	ld	a,c		; C:[5-3]
putcc@:	ld	de,_cc		; A:[5-3]	nz,z,nc,c,po,pe,p,m
	rrca
	rrca
	rrca
	and	7
putcc_:	push	bc	; put (de+a*3)-
	ld	b,a
	add	a,a
	add	a,b
	add	a,e
	ld	e,a
	ld	a,d
	adc	a,0
	ld	d,a
	call	putlin
	pop	bc
	ret

putad:	inc	hl
	ld	a,(hl)
	call	puthex
	dec	hl
	ld	a,(hl)
	call	puthex
	inc	hl
	inc	hl
	;ld	a,'h'
	;jp	putch
	ret

ldnym:	ld	de,_ld

lintab:	call	putlin
	jp	puttab

; ** ASM LOGIC **

; In: 	HL points to input string
asm_main:
	ld	b, 0
	ld	de, imm_unprefx
	call	parse_imm
 	cp	0
 	ret	nz
 	ld	b, 0EDh
 	ld	de, imm_edprefx
 	call	parse_imm
 	cp	0
 	ret	nz
 	ld	b, 0DDh
 	ld	de, imm_ddprefx
 	call	parse_imm
 	cp	0
 	ret	nz
 	ld	b, 0FDh
 	ld	de, imm_fdprefx
 	call	parse_imm
 	cp	0
 	ret	nz
 	ld	de, twostages
 	call	parse_grd
 	ret

; In:	DE points to last reading position
; Out:	DE points to next string pattern
; Use:	A, DE, F
; Cmt:	used only by 1 stage(imm) functions
next_imm:
	ld	a, (de)
	samejr	'$', next_imm_ret
	inc	de
	jr	next_imm
next_imm_ret:
	inc	de			; skip opcode and we reach
	inc	de			; the next parse string
	ret

; In: 	HL points to input string
;	DE points to parse table
;	B is prefix, if 0 no prefix
; Out: 	writes opcode if matched
;      	A != 0 if match
; Use:	DE, A, B, F
; Cmt: 	parses 1 stage opcodes
parse_imm:
	push	hl
parse_imm_loop:
	ld	a, (de)
	; check if table is ended
	samejr	0, mismatch_ret
	samejr	'$', parse_imm_chk	; Current opcode matches
	cp	(hl)
	jr	nz, parse_imm_next	; Current opcode doesn't match
	inc	de
	inc	hl
	jr	parse_imm_loop
parse_imm_next:
	call	next_imm
	pop	hl
	push	hl
	jr	parse_imm_loop
parse_imm_chk:
	; user string ended too_
	ld	a, (hl)
	cp	'$'
	jr	nz, parse_imm_next
	; is there a prefix to write_
	ld	a, b
	samejr	0, parse_imm_matched
	wrtb	a
parse_imm_matched:
	inc	de
	; getting and writing the byte machine code
	ld	a, (de)
	wrtb	a
	ld	a, 1			; A != 0, match occurred
	pop	hl
	ret

; Used by the all parser engine functions
; for invalid instructions
mismatch_ret:
	xor	a			; no match, set A = 0
	pop	hl
	ret

; In:	DE points to last reading position
; Out:	DE points to next string pattern
; Use:	A, DE, F
; Cmt:	used only by 2 stage(grd) functions
next_grd:
	inc	de
	ld	a, (de)
	samejr	'$', next_grd_ret
	jr	next_grd
next_grd_ret:
	inc	de			; skip jump address and we reach
	inc	de			; the next parse string
	inc	de
	ret

; In: 	HL points to input string
;	DE points to parse table
; Out: 	writes opcode if matched
;      	A != 0 if match
; Use:	BC, DE, IX, A, F
; Cmt: 	parses 2 stages opcodes
parse_grd:
	push	hl
	ld	ix, 0
parse_grd_loop:
	ld	a, (de)
	; check if table is ended
	samejr	0, mismatch_ret
	samejr	'$', parse_grd_2nd	; Ok now we jump to second stage parsing
	cp	(hl)
	jr	nz, parse_grd_next	; Current opcode doesn't match
	inc	de
	inc	hl
	jr	parse_grd_loop
parse_grd_next:
	call	next_grd
	pop	hl
	push	hl
	jr	parse_grd_loop
parse_grd_2nd:
	; if HL points to a space continue, otherwise return (no match)
	ld	a, (hl)
	cp	' '
	jr	nz, mismatch_ret
	inc	hl			; skip space
	inc	sp			; take rid of old input string position
	inc	sp
	inc	de			; now de points to jump label
	ld	a, (de)			; LSB
	ld 	c, a
	inc	de
	ld	a, (de)			; MSB
	ld	b, a
	add	ix, bc
	jp	(ix)

; In: 	HL points to current addr
;	DE points to new addr
; Out: 	CY=0 and A is displacement
;	CY=1 if displacement over/under flow
; Use:	HL, A, F
; Cmt: 	calculate displacement handling
;	over/under flow
get_displ:
	ex	de, hl
	or	a			; clear carry
	sbc	hl, de			; (new addr) - (current addr)
	jp	m, get_displ_neg
	ld	a, h
	cp	0
	jr	nz, get_displ_exit
	ld	a, l
	cp	128
	jr	nc, get_displ_exit
	jr	get_displ_ret
get_displ_neg:
	ld	a, h
	cp	0FFh
	jr	nz, get_displ_exit
	ld	a, l
	cp	128
	jr	c, get_displ_exit
get_displ_ret:
	sub	2
	jp	pe, get_displ_exit	; if overflow exit
	ex	de, hl
	or	a			; clear carry
	ret
get_displ_exit:
	scf
	ret

; In:	HL points to search string
;	DE points to table
; Out:	B = -1 if not found
;	B = index of string in tab
;	if B != -1 then HL is altered
; Use:	A, B, HL, DE, F
; Cmt: 	Returns index of string
;	in table
get_itab:
	ld	b, 0			; index
	push	hl
get_itab_loop:
	ld	a, (de)
	samejr	0, get_itab_notfound
	samejr	'$', get_itab_chk
	cp	(hl)
	jr	nz, get_itab_next
	inc	hl
	inc	de
	jr	get_itab_loop
get_itab_chk:
	ld	a, (hl)
	samejr	'$', get_itab_ret
	samejr	',', get_itab_inc
	jr	get_itab_count
get_itab_inc:
	inc	hl
get_itab_ret:
	inc	sp
	inc	sp
	ret
get_itab_notfound:
	ld	b, -1
	pop	hl
	ret
get_itab_next:
	inc	de
	ld	a, (de)
	cp	'$'
	jr	nz, get_itab_next
get_itab_count:
	inc	b
	pop	hl
	push	hl
	inc	de
	jr	get_itab_loop

; *********** Second Stage Parsing Jump labels **************

; HL -> {ex. DJNZ [0]105}
parse_djnz:
	call 	hexcheck_wrd		; in DE we have the hex number
	jr	c, djnz_mismatch
	; Input string must end, otherwise mismatch!
	ld	a, (hl)
	cp	'$'
	jr	nz, djnz_mismatch
	; Calculate displacement
	ld	hl, (adres)
	call	get_displ
	jr	c, djnz_mismatch
	; NOP for meaningful DJNZ
	samejr	0FFh, djnz_nop
	samejr	0FEh, djnz_nop
	; Write opcode
	wrtb 	10h
	; Write displacement
	wrtb 	a
	ld	a, 1			; A != 0 match
	ret
djnz_mismatch:
	xor	a
	ret
djnz_nop:
	wrtb	0
	ld	a, 1
	ret

; HL -> {ex. JR [C],0105}
parse_jr:
	ld	de, _cc2
	call	get_itab
	ld	a, b
	samejr	-1, jr_d
	cp	4
	jr	nc, jr_mismatch		; only allowed NZ, Z, NC, C
; HL -> (ex. JR C,[0]105)
jr_cc:
	call	hexcheck_wrd
	jr	c, jr_mismatch
	; Input string must end, otherwise mismatch!
	ld	a, (hl)
	cp	'$'
	jr	nz, jr_mismatch
	; Calculate displacement
	ld	hl, (adres)
	call	get_displ
	jr	c, jr_mismatch
	ex	af, af'			; save displacement
	; NOP for meaningful JR
	samejr	0FFh, jr_nop
	samejr	0FEh, jr_nop
	; Write opcode
	ld	a, b
	add	a, 4			; y = index + 4
	ld	b, a
	xor	a			; x = 0; z = 0
	yfrom	b
	wrtb	a			; write first byte
	; Write displacement
	ex	af, af'			; restore displacement
	wrtb 	a
	ld	a, 1			; A != 0 match
	ret
; HL -> (ex. JR [0]105)
jr_d:
	call	hexcheck_wrd
	jr	c, jr_mismatch
	; Input string must end, otherwise mismatch!
	ld	a, (hl)
	cp	'$'
	jr	nz, jr_mismatch
	; Calculate displacement
	ld	hl, (adres)
	call	get_displ
	jr	c, jr_mismatch
	; NOP for meaningful JR
	samejr	0FFh, jr_nop
	samejr	0FEh, jr_nop
	; Write opcode
	wrtb 	18h
	; Write displacement
	wrtb 	a
	ld	a, 1			; A != 0 match
	ret
jr_mismatch:
	xor	a
	ret
jr_nop:
	wrtb	0
	ld	a, 1
	ret

; HL -> {ex. LD [D]E,0105}
parse_ld:
	ld	(tmp_ptr), hl		; very important to reset HL for ld group
	ld	de, _rp
	call	get_itab
	ld	a, b
	samejp	-1, ld_nnreg
; HL -> {ex. LD DE,[0]105}
ld_rpnn:
	call	hexcheck_wrd
	jr	c, ld_rp_nnpar
	; Input string must end, otherwise mismatch!
	ld	a, (hl)
	cp	'$'
	jp	nz, ld_mismatch
	; write 1st fetch opcode
	and	3fh			; x = 0
	setz	1
	and	0f7h			; q = 0
	pfrom	b
	wrtb	a			; write opcode
	; write 2nd fetch opcode
	wrtb	e
	wrtb	d
	ld	a, 1
	ret
; HL -> {ex. LD DE,[(]0105)}
ld_rp_nnpar:
	ld	a, b
	samejp	2, ld_regnn		; for HL use regnn
	ld	a, (hl)
	cp	'('
	jp	nz, ld_mismatch
	inc	hl
	call	hexcheck_wrd
	jp	c, ld_mismatch
	ld	a, (hl)
	cp	')'
	jp	nz, ld_mismatch
	inc	hl
	ld	a, (hl)
	cp	'$'
	jp	nz, ld_mismatch
	; write ED prefix and 1st fetch opcode
	wrtb	0EDh
	setx	1
	setq	1
	pfrom	b
	setz	3
	wrtb	a
	; write 2nd fetch opcode
	wrtb	e
	wrtb	d
	ld	a, 1
	ret
; HL -> {ex. LD [(]1234),HL}
ld_nnreg:
	ld	a, (hl)
	cp	'('
	jp	nz, ld_regnn
	inc	hl
	call	hexcheck_wrd
	jp	c, ld_rn
 	ld	a, (hl)
 	cp	')'
 	jp	nz, ld_mismatch
 	inc	hl
 	ld	a, (hl)
 	cp	','
 	jp	nz, ld_mismatch
 	inc	hl
 	push	de			; save nn word for after
 	ld	de, _ldr
 	call	get_itab
	pop	de			; restore nn word
	ld	a, b
	samejr	-1, ld_nnrp		; reg could exist in another table_
 	add	a, 2			; _ldr table index starts from 2
 	ld	b, a
	; write first byte opcode
	and	3fh			; x = 0
	setz	2
	and	0f7h			; q = 0
	pfrom	b
	wrtb	a
	; write nn word
	wrtb	e
	wrtb	d
	ld	a, 1
	ret
; HL -> {ex. LD (1234),[B]C}
ld_nnrp:
	push	de
	ld	de, _rp
	call	get_itab
	pop	de
	ld	a, b
	samejr	-1, ld_nnixiy
	; write ED prefix and first byte opcode
	wrtb	0EDh
	setx	1
	pfrom	b
	and	0f7h			; q = 0
	setz	3
	wrtb	a
	; write nn word
	wrtb	e
	wrtb	d
	ld	a, 1
	ret
; HL -> {ex. LD (1234),[I]X or LD (1234),[I]Y}
ld_nnixiy:
	ld	a, (hl)
	cp	'I'
	jp	nz, ld_mismatch
	inc	hl
	ld	a, (hl)
	samejr	'X', ld_nnix
	samejr	'Y', ld_nniy
	jp	ld_mismatch
ld_nnix:
	ld	b, 0DDh
	jr	ld_nnixiy2
ld_nniy:
	ld	b, 0FDh
ld_nnixiy2:
	inc	hl
	ld	a, (hl)
	cp	'$'
	jp	nz, ld_mismatch
	; write prefix and first byte opcode
	wrtb	b
	wrtb	22h
	; write nn word
	wrtb	e
	wrtb	d
	ld	a, 1
	ret
; HL -> {ex. LD [A],(1234)}
ld_regnn:
	ld	hl, (tmp_ptr)
	ld	de, _ldr
	call	get_itab
	ld	a, b
	samejr	-1, ld_ixiy
	add	a, 2			; _ldr table index starts from 2
 	ld	b, a
	ld	a, (hl)
	cp	'('
	jp	nz, ld_rn
	inc	hl
	call	hexcheck_wrd
	jp	c, ld_rn
	ld	a, (hl)
 	cp	')'
 	jp	nz, ld_mismatch
	inc	hl
	ld	a, (hl)
	cp	'$'
	jp	nz, ld_mismatch
	; write first byte opcode
	and	3fh			; x = 0
	setz	2
	setq	1			; q = 1
	pfrom	b
	wrtb	a
	; write nn word
	wrtb	e
	wrtb	d
	ld	a, 1
	ret
; IX or IY instructions_ (no par.)
ld_ixiy:
	ld	a, (hl)
	cp	'I'
	jp	nz, ld_rn
	inc	hl
	ld	a, (hl)
	samejr	'X', ld_ixnn
	samejr	'Y', ld_iynn
	jp	ld_mismatch
ld_ixnn:
	ld	a, 0DDh
	ld	(tmp_var), a
	ld	de, _rx			; usefull for ixh... instructions
	jr	ld_ixiy2
ld_iynn:
	ld	a, 0FDh
	ld	(tmp_var), a
	ld	de, _ry			; usefull for iyh... instructions
ld_ixiy2:
	inc	hl
	ld	a, (hl)
	cp	','
	jp	nz, ld_ixyhlr
	inc	hl
; HL -> {ex. LD IX,[0]105 or LD IY,[0]105}
ld_ixiynn:
	call	hexcheck_wrd
	jr	c, ld_ixiy_nnpar
	ld	a, (hl)
	cp	'$'
	jp	nz, ld_mismatch
	; write prefix and first byte opcode
	ld	a, (tmp_var)
	wrtb	a
	wrtb	21h
	; write nn word
	wrtb	e
	wrtb	d
	ld	a, 1
	ret
; HL -> {ex. LD IX,[(]1234) or LD IY,[(]1234)}
ld_ixiy_nnpar:
	ld	a, (hl)
	cp	'('
	jp	nz, ld_mismatch
	inc	hl
	call	hexcheck_wrd
	jp	c, ld_mismatch
	ld	a, (hl)
	cp	')'
	jp	nz, ld_mismatch
	inc	hl
	ld	a, (hl)
	cp	'$'
	jp	nz, ld_mismatch
	; write prefix and first byte opcode
	ld	a, (tmp_var)
	wrtb	a
	wrtb	2Ah
	; write nn word
	wrtb	e
	wrtb	d
	ld	a, 1
	ret
; HL -> {ex. LD [A],12}
ld_rn:
	ld	hl, (tmp_ptr)
	ld	de, _r
	call	get_itab
	ld	a, b
	samejp	-1, ld_ixiydr
	ld	a, (hl)			; Fix ambiguity
	cp	'A'
	jr	nc, ld_rnchk
ld_rn2:
	call	hexcheck_byt
	jr	c, ld_rr
	ex	af, af'			; save n byte
	ld	a, (hl)
	cp	'$'
	jp	nz, ld_mismatch
	; write first byte opcode
	and	3fh			; x = 0
	setz	6
	yfrom	b
	wrtb	a
	; write n byte
	ex	af, af'			; restore n byte
	wrtb	a
	ld	a, 1
	ret
ld_rnchk:
	cp	'F'
	jr	nc, ld_rn2
	push	hl
	inc	hl
	ld	a, (hl)
	pop	hl
	cp	'$'
	jr	nz, ld_rn2
; HL -> {ex. LD A,[B]}
ld_rr:
	push	bc
	ld	de, _r
	call	get_itab
	ld	d, b			; d = z index
	ld	a, b
	pop	bc			; b = y index
	samejr	-1, ld_r_ixiyd
	setx	1
	yfrom	b
	setz	d
	; write opcode
	wrtb	a
	ld	a, 1
	ret
; HL -> {ex. LD A,[(]IX+06) or LD A,[(]IY+06)}
ld_r_ixiyd:
	ld	a, b
	samejp	6, ld_mismatch			; y != 6
	ld	a, (hl)
	cp	'('
	jp	nz, ld_rixyhl
	inc	hl
	ld	a, (hl)
	cp	'I'
	jp	nz, ld_mismatch
	inc	hl
	ld	a, (hl)
	samejr	'X', ld_r_ixd
	samejr	'Y', ld_r_iyd
	jp	ld_mismatch
ld_r_ixd:
	ld	a, 0DDh
	ld	(tmp_var), a
	jr	ld_r_ixiyd2
ld_r_iyd:
	ld	a, 0FDh
	ld	(tmp_var), a
; HL -> {ex. LD A,(I[X]+06) or LD A,(I[Y]+06)}
ld_r_ixiyd2:
	inc	hl
	ld	a, (hl)
	samejr	'+', ld_r_ixiydp
	samejr	'-', ld_r_ixiydm
	jp	ld_mismatch
ld_r_ixiydp:
	inc	hl
	call	hexcheck_byt
	ld	d, a				; displacement byte
	jp	c, ld_mismatch
	cp	080h
	jp	nc, ld_mismatch			; d < 128
	jr	ld_r_ixiyd3
ld_r_ixiydm:
	inc	hl
	call	hexcheck_byt
	jp	c, ld_mismatch
	cp	081h
	jp	nc, ld_mismatch			; d < 129
	neg
	ld	d, a				; displacement byte
ld_r_ixiyd3:
	ld	a, (hl)
	cp	')'
	jp	nz, ld_mismatch
	inc	hl
	ld	a, (hl)
	cp	'$'
	jp	nz, ld_mismatch
	; write prefix and first byte opcode
	ld	a, (tmp_var)			; DD or FD
	wrtb	a
	setx	1
	setz	6
	yfrom	b
	wrtb	a
	; write displacement byte
	wrtb	d
	ld	a, 1
	ret
; HL -> {ex. LD [(]IX+06),A or LD [(]IY+06),A}
ld_ixiydr:
	ld	hl, (tmp_ptr)
	ld	a, (hl)
	cp	'('
	jp	nz, ld_mismatch
	inc	hl
	ld	a, (hl)
	cp	'I'
	jp	nz, ld_mismatch
	inc	hl
	ld	a, (hl)
	samejr	'X', ld_ixdr
	samejr	'Y', ld_iydr
	jp	ld_mismatch
ld_ixdr:
	ld	a, 0DDh
	ld	(tmp_var), a
	jr	ld_ixiydr2
ld_iydr:
	ld	a, 0FDh
	ld	(tmp_var), a
; HL -> {ex. LD (I[X]+06),A or LD (I[Y]+06),A}
ld_ixiydr2:
	inc	hl
	ld	a, (hl)
	samejr	'+', ld_ixiydrp
	samejr	'-', ld_ixiydrm
	jp	ld_mismatch
ld_ixiydrp:
	inc	hl
	call	hexcheck_byt
	ld	d, a				; displacement byte
	jp	c, ld_mismatch
	cp	080h
	jp	nc, ld_mismatch			; d < 128
	jr	ld_ixiydr3
ld_ixiydrm:
	inc	hl
	call	hexcheck_byt
	jp	c, ld_mismatch
	cp	081h
	jp	nc, ld_mismatch			; d < 129
	neg
	ld	d, a				; displacement byte
ld_ixiydr3:
	ld	a, (hl)
	cp	')'
	jp	nz, ld_mismatch
	inc	hl
	ld	a, (hl)
	cp	','
	jp	nz, ld_mismatch
	inc	hl
	push	de				; save displacement
	ld	de, _r
	call	get_itab
	pop	de
	ld	a, b				; z index
	samejr	-1, ld_ixiydn
	samejp	6, ld_mismatch			; z != 6
	; write prefix and first byte opcode
	ld	a, (tmp_var)			; DD or FD
	wrtb	a
	setx	1
	setp	3
	and	0f7h				; q = 0
	setz	b
	wrtb	a
	; write displacement byte
	wrtb	d
	ld	a, 1
	ret
; HL -> {ex. LD (IX+06),[1]2 or LD (IY+06),[1]2}
ld_ixiydn:
	call	hexcheck_byt
	jr	c, ld_mismatch
	ld	b, a				; b = <immediate byte>
	ld	a, (hl)
	cp	'$'
	jr	nz, ld_mismatch
	; write prefix and first byte opcode
	ld	a, (tmp_var)			; DD or FD
	wrtb	a
	wrtb	36h
	; write displacement byte
	wrtb	d
	; write immediate byte
	wrtb	b
	ld	a, 1
	ret
ld_mismatch:
	xor	a
	ret
; HL -> {ex. LD IX[H],B or LD IY[L],B}
ld_ixyhlr:
	ld	a, (hl)
	samejr	'H', ld_ixyhr
	samejr	'L', ld_ixylr
	jr	ld_mismatch
ld_ixyhr:
	xor	a
	sety	4
	ld	(tmp_var2), a		; save y
	jr	ld_ixyhlr3
ld_ixylr:
	xor	a
	sety	5
	ld	(tmp_var2), a		; save y
ld_ixyhlr3:
	inc	hl
	ld	a, (hl)
	cp	','
	jr	nz, ld_mismatch
	inc	hl
	call	get_itab		; de is already set (check ld_ixiy)
	ld	a, b
	samejr	-1, ld_ixyhln		; z in b
	ld	a, (tmp_var)
	wrtb	a
	ld	a, (tmp_var2)		; set y
	setx	1
	setz	b
	wrtb	a
	ld	a, 1
	ret
; HL -> {ex. LD IXH,[1]2 or LD IYL,[1]2}
ld_ixyhln:
	call	hexcheck_byt
	jp	c, ld_mismatch
	ld	b, a
	ld	a, (hl)
	cp	'$'
	jp	nz, ld_mismatch
	ld	a, (tmp_var)
	wrtb	a		; write prefix
	ld	a, (tmp_var2)
	samejr	20h, ld_ixyhn	; 4 or 5 (shifted)
	samejr	28h, ld_ixyln
	jp	ld_mismatch
ld_ixyhn:
	wrtb	26h
	jr	ld_ixyhln2
ld_ixyln:
	wrtb	2Eh
ld_ixyhln2:
	wrtb	b
	ld	a, 1
	ret
; HL -> {ex. LD B,[I]XH or LD E,[I]YL}
ld_rixyhl:
	ld	a, (hl)
	cp	'I'
	jp	nz, ld_mismatch
	inc	hl
	ld	a, (hl)
	samejr	'X', ld_rixhl
	samejr	'Y', ld_riyhl
	jp	ld_mismatch
ld_rixhl:
	ld	a, 0DDh
	ld	(tmp_var), a
	ld	de, _rx
	jr	ld_rixyhl2
ld_riyhl:
	ld	a, 0FDh
	ld	(tmp_var), a
	ld	de, _ry
ld_rixyhl2:
	inc	hl
	ld	a, (hl)
	samejr	'H', ld_rixyh
	samejr	'L', ld_rixyl
	jp	ld_mismatch
ld_rixyh:
	xor	a
	setz	4
	ld	(tmp_var2), a		; save z
	jr	ld_rixyhl3
ld_rixyl:
	xor	a
	setz	5
 	ld	(tmp_var2), a		; save z
ld_rixyhl3:
	inc	hl
	ld	a, (hl)
	cp	'$'
	jp	nz, ld_mismatch
	; restore HL to starting pos
	ld	hl, (tmp_ptr)
	; de is already set
	call	get_itab
	ld	a, b			; y in b
	samejp	-1, ld_mismatch
	ld	a, (tmp_var)		; prefix
	wrtb	a
	ld	a, (tmp_var2)
	setx	1
	yfrom	b
	wrtb	a
	ld	a, 1
	ret
; HL -> {ex. ADD [A],B}
parse_add:
	xor	a
	ld	(tmp_var), a		; alu operation, y = 0
	jr	alu_a
; HL -> {ex. ADC [A],B}
parse_adc:
	ld	a, 1
	ld	(tmp_var), a		; alu operation, y = 1
	jr	alu_a
; HL -> {ex. SUB [B]}
parse_sub:
	ld	a, 2
	ld	(tmp_var), a		; alu operation, y = 2
	jr	alu_rr
; HL -> {ex. SBC [A],B}
parse_sbc:
	ld	a, 3
	ld	(tmp_var), a		; alu operation, y = 3
	jr	alu_a
; HL -> {ex. AND [B]}
parse_and:
	ld	a, 4
	ld	(tmp_var), a		; alu operation, y = 4
	jr	alu_rr
; HL -> {ex. XOR [B]}
parse_xor:
	ld	a, 5
	ld	(tmp_var), a		; alu operation, y = 5
	jr	alu_rr
; HL -> {ex. OR [B]}
parse_or:
	ld	a, 6
	ld	(tmp_var), a		; alu operation, y = 6
	jr	alu_rr
; HL -> {ex. CP [B]}
parse_cp:
	ld	a, 7
	ld	(tmp_var), a		; alu operation, y = 7
	jr	alu_rr
alu_a:
	ld	a, (hl)
	cp	'A'
	jr	nz, alu_hl
	inc	hl
	ld	a, (hl)
	cp	','
	jp	nz, alu_mismatch
	inc	hl
; HL -> {ex. <alu> A,[B]}
; HL -> {ex. <alu> [B]}
alu_rr:
	ld	de, _r
	call	get_itab
	ld	a, b			; z index in b
	samejr	-1, alu_n
	ld	a, (tmp_var)		; loading y
	ld	d, a
	; write opcode
	setx	2
	yfrom	d
	setz	b
	wrtb	a
	ld	a, 1
	ret
; HL -> {ex. <alu> A,[1]2}
; HL -> {ex. <alu> [1]2}
alu_n:
	call	hexcheck_byt
	jp	c, alu_ixiyd
	ld	b, a			; b = <immediate byte>
	ld	a, (hl)
	cp	'$'
	jp	nz, alu_mismatch
	ld	a, (tmp_var)		; loading y
	ld	d, a
	; write opcode
	setx	3
	yfrom	d
	setz	6
	wrtb	a
	; write immediate byte
	wrtb	b
	ld	a, 1
	ret
; HL -> {ex. <alu> [H]L,BC}
alu_hl:
	cp	'H'
	jp	nz, add_ixiyrp
	inc	hl
	ld	a, (hl)
	cp	'L'
	jp	nz, alu_mismatch
	inc	hl
	ld	a, (hl)
	cp	','
	jp	nz, alu_mismatch
	inc	hl
	ld	a, (tmp_var)
	samejr	0, add_hl
	samejr	1, adc_hl
	samejr	3, sbc_hl
	jp	alu_mismatch
; HL -> {ex. ADD HL,[B]C}
add_hl:
	xor	a
	ld	(tmp_var), a		; no prefix --> (tmp_var) = 0
	and	3fh			; x = 0
	setq	1
	setz	1
	ld	(tmp_var2), a		; just p value missing
	jr	alu_hl2
; HL -> {ex. ADC HL,[B]C}
adc_hl:
	ld	a, 0EDh
	ld	(tmp_var), a
	xor	a
	setx	1
	setq	1
	setz	2
	ld	(tmp_var2), a
	jr	alu_hl2
; HL -> {ex. SBC HL,[B]C}
sbc_hl:
	ld	a, 0EDh
	ld	(tmp_var), a
	xor	a
	setx	1
	and	0f7h			; q = 0
	setz	2
	ld	(tmp_var2), a
alu_hl2:
	ld	de, _rp
	call	get_itab
	ld	a, b
	samejp	-1, alu_mismatch
	ld	a, (tmp_var)
	samejr	0, alu_hl3
	; write prefix
	wrtb	a
alu_hl3:
	ld	a, (tmp_var2)
	pfrom	b
	; write opcode
	wrtb	a
	ld	a, 1
	ret
; HL -> {ex. <alu> A,[(]IX+d) or <alu> A,[(]IY+d) }
; HL -> {ex. <alu> [(]IX+d) or <alu> [(]IY+d) }
alu_ixiyd:
	ld	a, (hl)
	cp	'('
	jp	nz, alu_mismatch
	inc	hl
	ld	a, (hl)
	cp	'I'
	jp	nz, alu_mismatch
	inc	hl
	ld	a, (hl)
	samejr	'X', alu_ixd
	samejr	'Y', alu_iyd
	jp	alu_mismatch
alu_ixd:
	ld	a, 0DDh
	ld	(tmp_var2), a
	jr	alu_ixiyd2
alu_iyd:
	ld	a, 0FDh
	ld	(tmp_var2), a
alu_ixiyd2:
	inc	hl
	ld	a, (hl)
	samejr	'+', alu_ixiydp
	samejr	'-', alu_ixiydm
	jp	alu_mismatch
alu_ixiydp:
	inc	hl
	call	hexcheck_byt
	ld	d, a				; displacement byte
	jp	c, alu_mismatch
	cp	080h
	jp	nc, alu_mismatch		; d < 128
	jr	alu_ixiyd3
alu_ixiydm:
	inc	hl
	call	hexcheck_byt
	jp	c, alu_mismatch
	cp	081h
	jp	nc, alu_mismatch		; d < 129
	neg
	ld	d, a				; displacement byte
alu_ixiyd3:
	ld	a, (hl)
	cp	')'
	jp	nz, alu_mismatch
	inc	hl
	ld	a, (hl)
	cp	'$'
	jp	nz, alu_mismatch
	; write prefix and opcode
	ld	a, (tmp_var2)
	wrtb	a
	ld	a, (tmp_var)			; loading y
	ld	b, a
	setx	2
	yfrom	b
	setz	6
	wrtb	a
	; write displacement
	wrtb	d
	ld	a, 1
	ret
; HL -> {ex. ADD [I]X,BC or ADD [I]Y,IY }
add_ixiyrp:
	; only for ADD operation (y = 0)
	ld	a, (tmp_var)
	cp	0
	jp	nz, alu_mismatch
	ld	a, (hl)
	cp	'I'
	jp	nz, alu_mismatch
	inc	hl
	ld	a, (hl)
	samejr	'X', add_ixrp
	samejr	'Y', add_iyrp
	jp	alu_mismatch
add_ixrp:
	ld	a, 0DDh
	ld	(tmp_var), a
	jr	add_ixiyrp2
add_iyrp:
	ld	a, 0FDh
	ld	(tmp_var), a
add_ixiyrp2:
	inc	hl
	ld	a, (hl)
	cp	','
	jp	nz, alu_mismatch
	inc	hl
	ld	de, _rp
	call	get_itab
	ld	a, b
	samejr	-1, add_ixiy2
	samejr	2, alu_mismatch		; ADD IX,HL is illegal
	; write prefix and opcode
	ld	a, (tmp_var)
	wrtb	a
	and	3fh			; x = 0
	setq	1
	pfrom	b
	setz	1
	wrtb	a
	ld	a, 1
	ret
; HL -> {ex. ADD IX,[I]X or ADD IY,[I]Y }
add_ixiy2:
	ld	a, (hl)
	cp	'I'
	jr	nz, alu_mismatch
	ld	a, (tmp_var)
	samejr	0DDh, add_ix2
	samejr	0FDh, add_iy2
	jr	alu_mismatch
add_ix2:
	inc	hl
	ld	a, (hl)
	cp	'X'
	jr	nz, alu_mismatch
	jr	add_ixiy3
add_iy2:
	inc	hl
	ld	a, (hl)
	cp	'Y'
	jr	nz, alu_mismatch
add_ixiy3:
	inc	hl
	ld	a, (hl)
	cp	'$'
	jr	nz, alu_mismatch
	; write prefix and opcode
	ld	a, (tmp_var)
	wrtb	a
	ld	a, 29h
	wrtb	a
	ld	a, 1
	ret
alu_mismatch:
	xor	a
	ret

; HL -> {ex. DB [']test',12 }
parse_db:
	ld	de, tmp_buf
	push	de
	ld	b, strsz
	ld	a, '$'
	call	reset_buf
	pop	de
	ld	c, 0			; num total bytes
db_loop:
	ld	a, (hl)
	cp	39			; (') character
	jr	nz, db_num
db_str:
	inc	hl
	ld	a, (hl)
	samejr 	39, db_next
	ld	(de), a
	inc	de
	inc	c
	ld	a, c
	cp	bufsz - 1
	jr	nc, db_mismatch
	jr	db_str
db_next:
	inc	hl
db_numnext:
	ld	a, (hl)
	samejr	'$', db_write
	samejr	13, db_write
	cp	','
	jr	nz, db_mismatch
	inc	hl
	ld	a, (hl)
	samejr	'$', db_mismatch
	jr	db_loop
db_write:
	ld	de, tmp_buf
dx_wloop:
	ld	a, c
	samejr	0, dx_ret
	ld	a, (de)
	wrtb	a
	inc	de
	dec	c
	jr	dx_wloop
dx_ret:
	ld	a, 1
	ret
; HL -> {ex. DB [1]2,'prova' }
db_num:
	call	hexcheck_byt
	jr	c, db_mismatch
	ld	(de), a
	inc	de
	inc	c
	jr	db_numnext
db_mismatch:
	xor	a
	ret

; HL -> {ex. DW [1]234 }
parse_dw:
	ld	de, tmp_buf
	ld	b, strsz
	ld	a, '$'
	call	reset_buf
	ld	bc, tmp_buf
	xor	a
	ld	(tmp_var), a		; bytes counter
dw_loop:
	call	hexcheck_wrd
	jr	c, dw_mismatch
dw_next:
	ld	a, e
	ld	(bc), a
	inc	bc
	ld	a, d
	ld	(bc), a
	inc	bc
	ld	a, (tmp_var)
	add	a, 2
	ld	(tmp_var), a
	ld	a, (hl)
	samejr	'$', dw_write
	samejr	13, dw_write
	cp	','
	jr	nz, dw_mismatch
	inc	hl
	ld	a, (hl)
	samejr	'$', dw_mismatch
	jr	dw_loop
dw_write:
	ld	de, tmp_buf
	ld	a, (tmp_var)
	ld	c, a
	jp	dx_wloop
dw_mismatch:
	xor	a
	ret

; HL -> {ex. DS [F]F,'$' or DS [0]A,41 }
parse_ds:
	call	hexcheck_byt
	jr	c, ds_mismatch
	ld	b, a
	ld	a, (hl)
	cp	','
	jr	nz, ds_mismatch
	inc	hl
	ld	a, (hl)
	cp	39		; (') character
	jr	nz, ds_num
ds_char:
	inc	hl
	ld	a, (hl)
	ld	c, a
	inc	hl
	ld	a, (hl)
	cp	39
	jr	nz, ds_mismatch
	inc	hl
	ld	a, (hl)
ds_chk:
	samejr	'$', ds_write
	samejr	13, ds_write
	jr	ds_mismatch
ds_num:
	call	hexcheck_byt
	jr	c, ds_mismatch
	ld	c, a
	ld	a, (hl)
	jr	ds_chk
ds_write:
	ld	a, c
	wrtb	a
	djnz	ds_write
	ld	a, 1
	ret
ds_mismatch:
	xor	a
	ret

; In:	IX is return jump address
; Used to avoid code replication
parse_tab:
	ex	af, af'
	call	get_itab
	ld	a, b
	sameret	-1
	ex	af, af'
	jp	(ix)

; In:	HL points to string
;	DE points to table
;	A contains partial opcode values
; Out:	A = -1 if fail A = opcode otherwise
; Cmt:	Writes opcode using parsed register
; Use:	A, HL, DE, IX, B, F
parsew_rp:
	ld	ix, wrtb_setp
	jr	parse_tab
parsew_ccy:
parsew_ry:
	ld	ix, wrtb_sety
	jr	parse_tab
parsew_rz:
	ld	ix, wrtb_setz
	jr	parse_tab
parse_ccy:
parse_ry:
	ld	ix, just_sety
	jr	parse_tab
parse_rp:
	ld	ix, just_setp
	jr	parse_tab
parse_rz:
	ld	ix, just_setz
	jr	parse_tab
wrtb_setp:
	pfrom 	b
	wrtb	a
	ret
wrtb_sety:
	yfrom 	b
	wrtb	a
	ret
wrtb_setz:
	setz 	b
	wrtb	a
	ret
just_setp:
	pfrom	b
	ret
just_sety:
	yfrom	b
	ret
just_setz:
	setz 	b
	ret

; In:	HL points to string
; Out:	if CY=0, A=prefix byte
;	B=displ byte
;	if CY=1 parse failed
; Cmt:	HL is altered ONLY if CY=0
; Use:	A, A', B, HL, F
parse_ixiyd:
	push	hl
	ld	a, (hl)
	cp	'('
	jr	nz, parse_ixiyd_fail
	inc	hl
	ld	a, (hl)
	cp	'I'
	jr	nz, parse_ixiyd_fail
	inc	hl
	ld	a, (hl)
	samejr	'X', parse_ixd
	samejr	'Y', parse_iyd
parse_ixiyd_fail:
	pop	hl
	scf
	ret
parse_ixd:
	ld	a, 0DDh
	jr	parse_ixiyd2
parse_iyd:
	ld	a, 0FDh
parse_ixiyd2:
	ex	af, af'
	inc	hl
	ld	a, (hl)
	samejr	'+', parse_ixiydp
	samejr	'-', parse_ixiydm
	jr	parse_ixiyd_fail
parse_ixiydp:
	inc	hl
	call	hexcheck_byt
	ld	b, a				; displacement byte
	jr	c, parse_ixiyd_fail
	cp	080h
	jr	nc, parse_ixiyd_fail		; d < 128
	jr	parse_ixiyd3
parse_ixiydm:
	inc	hl
	call	hexcheck_byt
	jr	c, parse_ixiyd_fail
	cp	081h
	jr	nc, parse_ixiyd_fail		; d < 129
	neg
	ld	b, a				; displacement byte
parse_ixiyd3:
	ld	a, (hl)
	cp	')'
	jr	nz, parse_ixiyd_fail
	inc	hl
	ld	a, (hl)
	cp	'$'
	jr	nz, parse_ixiyd_fail
	or	a				; clear carry
	ex	af, af'				; prefix
	inc	sp
	inc	sp				; throw away old HL
	ret

; HL -> {ex. INC [H]L }
parse_inc:
inc_rp:
	and	3fh		; x = 0
	and	0f7h		; q = 0
	setz	3
	ld	de, _rp
	call	parsew_rp
	samejr	-1, inc_r
	ld	a, 1
	ret
; HL -> {ex. INC [A] }
inc_r:
	and	3fh		; x = 0
	setz	4
	ld	de, _r
	call	parsew_ry
	samejr	-1, inc_ixiyd
	ld	a, 1
	ret
; HL -> {ex. INC [(]IX+12) }
inc_ixiyd:
	call	parse_ixiyd
	jr	c, inc_mismatch
	wrtb	a		; prefix
	wrtb	34h		; opcode
	wrtb	b		; displacement
	ld	a, 1
	ret
inc_mismatch:
	xor	a
	ret

; HL -> {ex. DEC [H]L }
parse_dec:
dec_rp:
	and	3fh		; x = 0
	setq	1
	setz	3
	ld	de, _rp
	call	parsew_rp
	samejr	-1, dec_r
	ld	a, 1
	ret
; HL -> {ex. DEC [A] }
dec_r:
	and	3fh		; x = 0
	setz	5
	ld	de, _r
	call	parsew_ry
	samejr	-1, dec_ixiyd
	ld	a, 1
	ret
; HL -> {ex. DEC [(]IX+12) }
dec_ixiyd:
	call	parse_ixiyd
	jr	c, dec_mismatch
	wrtb	a		; prefix
	wrtb	35h		; opcode
	wrtb	b		; displacement
	ld	a, 1
	ret
dec_mismatch:
	xor	a
	ret

; HL -> {ex. RET [Z] }
parse_retcc:
	setx	3
	and	0f8h		; z = 0
	ld	de, _cc2
	call	parsew_ccy
	samejr	-1, ret_mismatch
	ld	a, 1
	ret
ret_mismatch:
	xor	a
	ret

; HL -> {ex. POP [H]L or PUSH [B]C }
parse_pop:
	setx	3
	and	0f7h		; q = 0
	setz	1
	jr	stack_rp2
parse_push:
	setx	3
	and	0f7h		; q = 0
	setz	5
stack_rp2:
	ld	de, _rp2
	call	parsew_rp
	samejr	-1, stack_mismatch
	ld	a, 1
	ret
stack_mismatch:
	xor	a
	ret

parse_jp:
; HL -> {ex. JP [1]234 }
jp_nn:
	call	hexcheck_wrd
	jr	c, jp_ccnn
	ld	a, (hl)
	cp	'$'
	jr	nz, jp_mismatch
	wrtb	0C3h
	wrtb	e
	wrtb	d
	ld	a, 1
	ret
; HL -> {ex. JP [N]Z,1234 }
jp_ccnn:
	setx	3
	setz	2
	ld	de, _cc2
	call	parse_ccy
	samejr	-1, jp_mismatch
	ld	(tmp_var), a		; opcode
	call	hexcheck_wrd
	jr	c, jp_mismatch
	ld	a, (hl)
	cp	'$'
	jr	nz, jp_mismatch
	ld	a, (tmp_var)
	wrtb	a
	wrtb	e
	wrtb	d
	ld	a, 1
	ret
jp_mismatch:
	xor	a
	ret

parse_call:
; HL -> {ex. CALL [1]234 }
call_nn:
	call	hexcheck_wrd
	jr	c, call_ccnn
	ld	a, (hl)
	cp	'$'
	jr	nz, call_mismatch
	wrtb	0CDh
	wrtb	e
	wrtb	d
	ld	a, 1
	ret
; HL -> {ex. CALL [C], 1234 }
call_ccnn:
	setx	3
	setz	4
	ld	de, _cc2
	call	parse_ccy
	samejr	-1, call_mismatch
	ld	(tmp_var), a		; opcode
	call	hexcheck_wrd
	jr	c, call_mismatch
	ld	a, (hl)
	cp	'$'
	jr	nz, call_mismatch
	ld	a, (tmp_var)
	wrtb	a
	wrtb	e
	wrtb	d
	ld	a, 1
	ret
call_mismatch:
	xor	a
	ret

; HL -> {ex. RST [3]8 }
parse_rst:
	call	hexcheck_byt
	jr	c, rst_mismatch
	ld	b, a
	and	0C7h			; test if byte is valid
	cp	0
	jr	nz, rst_mismatch
	ld	a, b
	sra	a
	sra	a
	sra	a
	ld	b, a
	ld	a, (hl)
	cp	'$'
	jr	nz, rst_mismatch
	setx	3
	yfrom	b
	setz	7
	wrtb	a
	ld	a, 1
	ret
rst_mismatch:
	xor	a
	ret

; HL -> {ex. <ROT> [B]}
parse_rlc:
	xor	a
	ld	(tmp_var), a		; rot operation, y = 0
	jr	rot_rz
parse_rrc:
	ld	a, 1
	ld	(tmp_var), a		; rot operation, y = 1
	jr	rot_rz
parse_rl:
	ld	a, 2
	ld	(tmp_var), a		; rot operation, y = 2
	jr	rot_rz
parse_rr:
	ld	a, 3
	ld	(tmp_var), a		; rot operation, y = 3
	jr	rot_rz
parse_sla:
	ld	a, 4
	ld	(tmp_var), a		; rot operation, y = 4
	jr	rot_rz
parse_sra:
	ld	a, 5
	ld	(tmp_var), a		; rot operation, y = 5
	jr	rot_rz
parse_sll:
	ld	a, 6
	ld	(tmp_var), a		; rot operation, y = 6
	jr	rot_rz
parse_srl:
	ld	a, 7
	ld	(tmp_var), a		; rot operation, y = 7
rot_rz:
	ld	de, _r
	call	parse_rz
	samejr	-1, rot_ixiyd
	; write prefix
	wrtb	0CBh
	; z is already set
	and	3fh			; x = 0
	ex	af, af'
	ld	a, (tmp_var)
	ld	b, a
	ex	af, af'
	yfrom	b
	wrtb	a
	ld	a, 1
	ret
; HL -> {ex. <ROT> (IX+12) or <ROT> (IY+12) }
rot_ixiyd:
	call	parse_ixiyd
	jr	c, rot_mismatch
	wrtb	a			; DD or FD
	wrtb	0CBh
	wrtb	b			; displacement
	ld	a, (tmp_var)
	ld	b, a
	and	3fh			; x = 0
	yfrom	b
	setz	6
	wrtb	a
	ld	a, 1
	ret
rot_mismatch:
	xor	a
	ret

; HL -> {ex. IN [A],(A8) }
parse_in:
in_an:
	ld	de, _r
	call	get_itab
	ld	a, b
	samejp	-1, inout_mismatch
	ld	(tmp_var), a		; save y
	cp	7			; only A
	jr	nz, in_ryc
	ld	a, (hl)
	cp	'('
	jr	nz, inout_mismatch
	inc	hl
	ld	a, (hl)
	samejr	'C', in_ryc2		; fix (C) or (0C) ambiguity
	call	hexcheck_byt
	jr	c, in_ryc2
	ld	b, a
	ld	a, (hl)
	cp	')'
	jr	nz, inout_mismatch
	inc	hl
	ld	a, (hl)
	cp	'$'
	jr	nz, inout_mismatch
	wrtb	0DBh
	wrtb	b
	ld	a, 1
	ret
; HL -> {ex. IN [B],(C) }
in_ryc:
	ld	a, (tmp_var)
	samejr	6, inout_mismatch	; (HL) not allowed
	ld	b, a			; y value
	ld	a, (hl)
	cp	'('
	jr	nz, inout_mismatch
	inc	hl
in_ryc2:
	ld	a, (hl)
	cp	'C'
	jr	nz, inout_mismatch
	inc	hl
	ld	a, (hl)
	cp	')'
	jr	nz, inout_mismatch
	inc	hl
	ld	a, (hl)
	cp	'$'
	wrtb	0EDh
	setx	1
	yfrom	b
	and	0f8h			; z = 0
	wrtb	a
	ld	a, 1
	ret

inout_mismatch:
	xor	a
	ret

; HL -> {ex. OUT [(]A8),A }
parse_out:
out_na:
	ld	a, (hl)
	cp	'('
	jr	nz, inout_mismatch
	inc	hl
	ld	a, (hl)
	samejr	'C', out_cry		; fix (C) or (0C) ambiguity
	call	hexcheck_byt
	jr	c, out_cry
	ld	b, a			; port
	ld	a, (hl)
	cp	')'
	jr	nz, inout_mismatch
	inc	hl
	ld	a, (hl)
	cp	','
	jr	nz, inout_mismatch
	inc	hl
	ld	a, (hl)
	cp	'A'
	jr	nz, inout_mismatch
	inc	hl
	ld	a, (hl)
	cp	'$'
	jr	nz, inout_mismatch
	wrtb	0D3h
	wrtb	b
	ld	a, 1
	ret
; HL -> {ex. OUT ([C]),B }
out_cry:
	ld	a, (hl)
	cp	'C'
	jr	nz, inout_mismatch
	inc	hl
	ld	a, (hl)
	cp	')'
	jr	nz, inout_mismatch
	inc	hl
	ld	a, (hl)
	cp	','
	jr	nz, inout_mismatch
	inc	hl
	ld	de, _r
	call	get_itab
	ld	a, b
	samejr	-1, inout_mismatch
	samejr	6, inout_mismatch
	wrtb	0EDh
	setx	1
	yfrom	b
	setz	1
	wrtb	a
	ld	a, 1
	ret

; HL -> {ex. BIT [2],E }
parse_bit:
	xor	a
	setx	1
	ld	(tmp_var), a		; save x
	jr	bitop
; HL -> {ex. RES [7],B }
parse_res:
	xor	a
	setx	2
	ld	(tmp_var), a
	jr	bitop
; HL -> {ex. SET [1],A }
parse_set:
	xor	a
	setx	3
	ld	(tmp_var), a
bitop:
	ld	a, (hl)
	sub	'0'
	cp	8
	jp	nc, bitop_mismatch
	ld	b, a			; y in b
	ld	a, (tmp_var)
	yfrom	b
	ld	(tmp_var), a		; save x & y
	inc	hl
	ld	a, (hl)
	cp	','
	jr	nz, bitop_mismatch
	inc	hl
	ld	de, _r
	ld	a, (tmp_var)
	call	parse_rz
	samejr	-1, bitop_ixiyd
	wrtb	0CBh
	wrtb	a
	ld	a, 1
	ret
; HL -> {ex. <BITOP> 2,[(]IX+12) or <BITOP> 7,[(]IY-23) }
bitop_ixiyd:
	call	parse_ixiyd
	jr	c, bitop_mismatch
	wrtb	a			; DD or FD
	wrtb	0CBh
	wrtb	b
	ld	a, (tmp_var)		; x & y
	setz	6
	wrtb	a
	ld	a, 1
	ret
bitop_mismatch:
	xor	a
	ret

; HL -> {ex. MULUB [A],D}
parse_mulub:
	ld	a, (hl)
	cp	'A'
	jr	nz, mul_mismatch
	inc	hl
	ld	a, (hl)
	cp	','
	jr	nz, mul_mismatch
	inc	hl
	ld	de, _r
	call	parse_ry		; set y
	samejr	-1, mul_mismatch
	wrtb	0EDh
	setx	3
	setz	1
	wrtb	a
	ld	a, 1
	ret
; HL -> {ex. MULUW [H]L,DE}
parse_muluw:
	ld	a, (hl)
	cp	'H'
	jr	nz, mul_mismatch
	inc	hl
	ld	a, (hl)
	cp	'L'
	jr	nz, mul_mismatch
	inc	hl
	ld	a, (hl)
	cp	','
	jr	nz, mul_mismatch
	inc	hl
	ld	de, _rp
	call	parse_rp		; set p
	samejr	-1, mul_mismatch
	wrtb	0EDh
	setx	3
	and	0f7h			; q = 0
	setz	3
	wrtb	a
	ld	a, 1
	ret
mul_mismatch:
	xor	a
	ret
